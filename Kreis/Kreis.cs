﻿using System;
using System.Drawing;

namespace Kreise
{
    internal class Kreis
    {
        double radius = 0;
        Color farbe;
        private double x;
        private double y;

        public Kreis(double lx=0, double ly=0)
        {
            x = lx;
            y = ly;
            radius = 1;
            farbe = Color.White;

        }
        public double getFlaeche()
        {
            return (Math.PI * Math.Pow(radius, 2));
        }

        public void setFarbe(Color farbe)
        {
            this.farbe = farbe;
        }

        public void setRadius(double lr)
        {
            raduis = lr;

        }
        //static void Main(string[] args)
        //{
            //Console.WriteLine("Hello World!");
        //}
    }
}
